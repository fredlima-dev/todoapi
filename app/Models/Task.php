<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Task extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'due',
        'status',
        'priority',
        'duration'
    ];

    public static function validate($data){
        return Validator::make($data, [
            'title'    => 'required',
            'status'   => 'required|in:pending,doing,done',
            'priority' => 'required|in:low,medium,high'
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
